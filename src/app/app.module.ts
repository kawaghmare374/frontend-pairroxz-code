import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { TaskpageComponent } from './taskpage/taskpage.component';

import { HeadingComponent } from './heading/heading.component';
import { Router, RouterModule ,Routes} from '@angular/router';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';


  
 const routes: Routes=[

  {path : 'form', component:HeadingComponent},
  {path :'Cancle',component:HeadingComponent}
 
  
]; 



@NgModule({
  declarations: [
    AppComponent,
    TaskpageComponent,
    HeadingComponent,
   
  
   
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
