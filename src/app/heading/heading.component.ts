import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.css']
})
export class HeadingComponent implements OnInit {
  editForm:FormGroup;


  constructor(private route :Router , fb: FormBuilder) { 
    this.editForm = fb.group({
      name: ["", Validators.required]
  });
    
  }

  ngOnInit(): void {
  }

  

 /*  check()
  {
    alert("Add Task")
 
    this.AddTask=false
    this.route.navigate(['/AddTask']);
    
  } */

 
    

}
